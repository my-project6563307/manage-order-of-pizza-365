$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo Url gọi api
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    const gBASE_URL_DRINK ="http://203.171.20.210:8080/devcamp-pizza365/drinks";
    // Khai báo biến lưu tất cả đơn hàng
    var gAllOrders = [];
    // Khai báo biến lưu danh sách loại nước uống
    var gDrinkList = [];
    // Cấu hình data table với các thuộc tính
    var gColName = ["orderCode","kichCo","loaiPizza","idLoaiNuocUong","thanhTien", "hoTen","soDienThoai","trangThai","chitiet"];
    const gCOL_CODE = 0;
    const gCOL_SIZE = 1;
    const gCOL_TYPE = 2;
    const gCOL_DRINK = 3;
    const gCOL_PRICE = 4;
    const gCOL_NAME = 5;
    const gCOL_PHONE = 6;
    const gCOL_STATUS = 7;
    const gCOL_DETAIL = 8;
    var gOrderTable = $("#order-table").DataTable({
        columns: [
            {data: gColName[gCOL_CODE]},
            {data: gColName[gCOL_SIZE]},
            {data: gColName[gCOL_TYPE]},
            {data: gColName[gCOL_DRINK]},
            {data: gColName[gCOL_PRICE]},
            {data: gColName[gCOL_NAME]},
            {data: gColName[gCOL_PHONE]},
            {data: gColName[gCOL_STATUS]},
            {data: gColName[gCOL_DETAIL]}
        ],
        columnDefs: [
            {
                targets: gCOL_DETAIL,
                defaultContent: `<i class="fa-regular fa-pen-to-square fa-2x icon-edit"></i>
                <i class="fa-regular fa-trash-can fa-2x icon-delete"></i>`
            }
        ]
    });
    // Khai báo biến lưu Id cho đơn hàng đang cần xử lí
    var gId = "";
    // Khai báo biến lưu order code cho đơn hàng đang cần xử lí
    var gOrderCode = "";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    // Gọi hàm tải trang
    onPageLoading();
    // Gán sự kiện click cho nút nhấn lọc đơn hàng
    $(document).on("click","#btn-filter", function(){
        onBtnFilterClick();
    });
    // Gán sự kiện cho nút thêm đơn hàng 
    $(document).on("click","#btn-create-order", function(){
        onBtnCreateOrderClick();
    });
    // Gán sự kiện change khi chọn combo pizza trên modal 
    $(document).on("change","#select-pizza-size-create", function(){
        onChageSelectPizzaClick();
    });
    // Gán sự kiện nhấn nút xác nhận tạo đơn trên modal
    $(document).on("click","#btn-confirm-create", function(){
        onBtnConfirmCreateOrderClick();
    });
    // Gán sự kiện click cho icon edit
    $(document).on("click","#order-table .icon-edit", function(){
        onIconEditClick(this);
    });
    // Gán sự kiện cho nút nhấn xác nhận đơn hàng trên modal edit
    $(document).on("click","#btn-confirm", function(){
        onBtnConfirmOrderClick();
    });
    // Gán sự kiện cho nút nhấn huỷ bỏ đơn hàng trên modal edit
    $(document).on("click","#btn-cancel", function(){
        onBtnCancelOrderClick();
    });
    // Gán sự kiện cho icon xoá bỏ đơn hàng 
    $(document).on("click","#order-table .icon-delete", function(){
        onIconDeleteOrderClick(this);
    });
    // Gán sự kiện cho nút xác nhận xoá bỏ đơn hàng trên modal delete
    $(document).on("click","#btn-confirm-delete", function(){
        onBtnDeleteOrderClick();
    });
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        // Gọi Api lấy toàn bộ danh sách đơn hàng
        callApiGetAllOrder();
        // Gọi Api lấy toàn bộ danh sách nước uống
        callApiGetAllDrink();
        // Xử lí dữ liệu vào ô select nước uống
        handleDataDrinkToModal(gDrinkList);
        // Xử lí hiển thị dữ liệu lên bảng
        handleDataToTable(gAllOrders);
    };
    // Hàm xử lí sự kiện nhấn nút thêm đơn hàng
    function onBtnCreateOrderClick(){
        "use strict"
        console.log("Nút thêm đơn hàng được nhấn");
        // Hiển thị modal tạo đơn hàng khi nhấn nút
        $("#order-create-modal").modal("show");
    }
    // Hàm xử lí nhấn nút tạo đơn trên modal
    function onBtnConfirmCreateOrderClick(){
        "use strict"
        // Khai báo đối tượng đơn hàng cần tạo
        var vOrderObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        // B1 Thu thập dữ liệu trên form modal
        readDataOnCreateModal(vOrderObjectRequest);
        // B2 Kiểm tra dữ liệu
        var vValidateResult = validateDataCreateOrderObject(vOrderObjectRequest);
        // B3 Xử lí tạo đơn hàng
        if (vValidateResult){
            callApiCreateOrder(vOrderObjectRequest);
        }
    }
    // Hàm xử lí khi chọn combo pizza trên modal tạo đơn hàng
    function onChageSelectPizzaClick(){
        "use strict"
        var vValueSelectChange = $("#select-pizza-size-create").val();
        if (vValueSelectChange == "S"){
            console.log("Combo S");
            loadComboToModal (20 , 2, 200, 2, 150000);
        }
        if (vValueSelectChange == "M"){
            console.log("Combo M");
            loadComboToModal (25 , 4, 300, 3, 200000);
        }
        if (vValueSelectChange == "L"){
            console.log("Combo L");
            loadComboToModal (30 , 8, 500, 4, 250000);
        }
        if(vValueSelectChange == 0){
            loadComboToModal();
        }
    }
    // Hàm xử lí sự kiện nhấn nút confirm trên modal edit
    function onBtnConfirmOrderClick(){
        "use strict"
        // B1 + B2 thu thập dữ liệu và kiểm tra id ( đã thu thập và kiểm tra khi nhấn nút chi tiểt)
        // B3 Gọi Api confirm đơn hàng
        console.log("Nút confirm được nhấn");
        console.log("id: " + gId);
        callApiConfirmOrder(gId);
    }
    // Hàm xử lí sự kiện nhấn nút cancel trên modal edit
    function onBtnCancelOrderClick(){
        "use strict"
        // B1 + B2 thu thập dữ liệu và kiểm tra id ( đã thu thập và kiểm tra khi nhấn nút chi tiểt)
        // B3 Gọi Api cancel đơn hàng
        console.log("Nút cancel được nhấn");
        console.log("id: " + gId);
        callApiCancelOrder(gId);
    }
    // Hàm xử lí sự kiện nhấn icon edit
    function onIconEditClick(paramElement){
        "use strict"
        console.log("Icon edit được nhấn");
        // B1 lấy dữ liệu của hàng được nhấn nút
        var vDataRow = getDataRow(paramElement);
        // B2 gán dữ liệu id và order code
        getIdAndOrderCode(vDataRow);
        // B3 Kiểm tra dữ liệu
        var vValidateResult = validateDataIdAndOrderCode(gId,gOrderCode);
        if (vValidateResult){
        // B4 Gọi Api lấy thông tin order
        callApiGetOrderbyOrderCode(gOrderCode);
        $("#order-edit-modal").modal("show");
        }
    };
    // Hàm xử lí sự kiện nhấn icon delete
    function onIconDeleteOrderClick(paramElement){
        "use strict"
        console.log("Icon delete được nhấn");
        // B1 lấy dữ liệu của hàng được nhấn nút
        var vDataRow = getDataRow(paramElement);
        // B2 gán dữ liệu id và order code
        getIdAndOrderCode(vDataRow);
        // B3 Kiểm tra dữ liệu
        var vValidateResult = validateDataIdAndOrderCode(gId,gOrderCode);
        if (vValidateResult){
        // B4 Hiển thị modal xoá
        $("#order-delete-modal").modal("show");
        };
    };
    // Hàm xử lí nhấn nút xác nhận xoá trên modal edit
    function onBtnDeleteOrderClick(){
        "use strict"
        callApiDeleteOrderbyId(gId);
    }
    //Hàm xử lí sự kiện nhấn nút lọc;
    function onBtnFilterClick(){
        "use strict"
        console.log("Nút lọc đơn hàng được nhấn");
        // tạo đối tượng lưu thông tin lọc
        var vFilterObj = {
            trangThai: "",
            loaiPizza: ""
        };
        // B1 thu thập dữ liệu cho đối tượng;
        readDataToFilterObj(vFilterObj);
        // B2 Kiểm tra dữ liệu
        // Bước này không cần kiểm tra
        // B3 Xử lí lọc dữ liệu
        console.log(vFilterObj);
        var vFilterArr = filterData(vFilterObj);
        console.log(vFilterArr);
        if (vFilterArr.length > 0){
            handleDataToTable(vFilterArr);
        }
        else{
            alert("Không tìm thấy đơn hàng nào khớp!");
            // handleDataToTable(gAllOrders);
        }
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm gọi Api lấy toàn bộ danh sách order
    function callApiGetAllOrder(){
        "use strict"
        $.ajax({
            url: gBASE_URL,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                gAllOrders = res; // Lưu dữ liệu trả về vào biến toàn cục
            },
            error: function(context){
                console.log(context.responseText);
            }
        });
    }
    // Gọi Api cập nhật confirm đơn hàng
    function callApiConfirmOrder(paramId){
        "use strict"
        var vObjectRequest = {
            trangThai: "confirmed"
        };
        var vObjectJsonRequest = JSON.stringify(vObjectRequest);
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            type: "PUT",
            contentType: "application/json",
            data: vObjectJsonRequest,
            async: false,
            success: function(res){
                console.log(res);
                alert("Đã xác nhận đơn hàng thành công");
                window.location.reload();
            },
            error: function(context){
                console.log(context.responseText);
                alert("Lỗi khi xác nhận đơn hàng");
            }
        })
    }
    // Gọi Api cập nhật cancel đơn hàng
    function callApiCancelOrder(paramId){
        "use strict"
        var vObjectRequest = {
            trangThai: "cancel"
        };
        var vObjectJsonRequest = JSON.stringify(vObjectRequest);
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            type: "PUT",
            contentType: "application/json",
            data: vObjectJsonRequest,
            async: false,
            success: function(res){
                console.log(res);
                alert("Đã huỷ đơn hàng thành công");
                window.location.reload();
            },
            error: function(context){
                console.log(context.responseText);
                alert("Lỗi khi huỷ đơn hàng");
            }
        })
    }
    // Gọi Api tạo đơn hàng
    function callApiCreateOrder(paramObjOrder){
        "use strict"
        var vObjectJsonRequest = JSON.stringify(paramObjOrder);
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json",
            data: vObjectJsonRequest,
            success: function(res){
                console.log(res);
                alert("Đã tạo đơn hàng thành công");
                window.location.reload();
            },
            error: function(context){
                console.log(context.responseText);
                alert("Lỗi khi tạo đơn hàng, hãy kiểm tra lại");
            }
        })
    }
    // Gọi APi lấy order bằng order code
    function callApiGetOrderbyOrderCode(paramOrderCode){
        "use strict"
        $.ajax({
            url: gBASE_URL + "/" + paramOrderCode,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                handleDataToModal(res);
            },
            error: function(context){
                console.log(context.responseText);
            }
        })
    };
    // Gọi APi xoá order bằng id
    function callApiDeleteOrderbyId(paramId){
        "use strict"
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            type: "DELETE",
            dataType: "json",
            success: function(){
                alert("Xoá đơn hàng thành công!");
                window.location.reload();
            },
            error: function(context){
                console.log(context.responseText);
                alert("Xoá đơn hàng thất bại, hãy kiểm tả lại!");
            }
        })
    };
    // Hàm thu thâp dữ liệu từ form create order modal
    function readDataOnCreateModal(paramObjOrder){
        "use strict"
        paramObjOrder.kichCo = $("#select-pizza-size-create").val();
        paramObjOrder.duongKinh = $("#input-duong-kinh-create").val();
        paramObjOrder.suon = $("#input-suon-create").val();
        paramObjOrder.salad = $("#input-salad-create").val();
        paramObjOrder.loaiPizza = $("#select-pizza-type-create").val();
        paramObjOrder.idVourcher = $("#input-voucher-create").val();
        paramObjOrder.idLoaiNuocUong = $("#select-drink-create").val();
        paramObjOrder.soLuongNuoc = $("#input-number-drink-create").val();
        paramObjOrder.hoTen = $("#input-name-create").val();
        paramObjOrder.thanhTien = $("#input-price-create").val();
        paramObjOrder.email = $("#input-email-create").val();
        paramObjOrder.soDienThoai = $("#input-phone-create").val();
        paramObjOrder.diaChi = $("#input-address-create").val();
        paramObjOrder.loiNhan = $("#input-message-create").val();
    }
    // Hàm kiểm tra dữ liệu khi nhấn nút tạo xác nhận tạo đơn
    function validateDataCreateOrderObject(paramObjOrder){
        "use strict"
        if (paramObjOrder.kichCo == 0){
            alert("Hãy chọn Combo Pizza!");
            $("#select-pizza-size-create").focus();
            return false;
        }
        if (paramObjOrder.loaiPizza == 0){
            alert("Hãy chọn một loại Pizza");
            $("#select-pizza-type-create").focus();
            return false;
        }
        if (paramObjOrder.idLoaiNuocUong == 0){
            alert("Hãy chọn một loại nước uống");
            $("#select-drink-create").focus();
            return false;
        }
        if (paramObjOrder.idVourcher == ""){
            // Rỗng thì bỏ qua
        }
        else if(isNaN(paramObjOrder.idVourcher)) {
                alert("Voucher nhập vào phải là số");
                return false;
        }
        if (paramObjOrder.hoTen == ""){
            alert("Hãy nhập họ và tên");
            return false;
        }
        if (paramObjOrder.email == ""){
            // Rỗng thì bỏ qua  
        }
        else {
            if ((validateEmail(paramObjOrder.email) == false)){
                return false;
            }
        }
        if (paramObjOrder.soDienThoai == ""){
            alert("Hãy nhập số điện thoại");
            return false
        }
        if (paramObjOrder.diaChi == ""){
            alert("Hãy nhập địa chỉ")
            return false;
        }
        return true;
    }
    // Hàm kiểm tra email
    function validateEmail(paramEmail){
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(paramEmail.match(mailformat)){
            return true;
        }
        else{
            alert("Chưa đúng định dạng email!");
            return false;
        }
    }
    // Hàm gán dữ liệu cho id và order code
    function getIdAndOrderCode(paramObj){
        "use strict"
        gId = paramObj.id;
        gOrderCode = paramObj.orderCode;
        console.log("id: " + gId);
        console.log("Order Code: " + gOrderCode);
    } ;
    // Hàm kiểm tra id và order của dòng được chọn
    function validateDataIdAndOrderCode(paramId, paramOrderCode){
        "use strict"
        if(paramId == "" || paramId == null){
            alert("Không tìm thấy id đơn hàng");
            return false;
        }
        if(paramOrderCode == "" || paramOrderCode == null){
            alert("Không tìm thấy ordercode đơn hàng");
            return false;
        }
        return true;
    }
    // Hàm hiển thị thông tin lên modal
    function handleDataToModal(paramObj){
        "use strict"
        $("#input-order-code").val(paramObj.orderCode);
        $("#select-pizza-size").val(paramObj.kichCo);
        $("#input-duong-kinh").val(paramObj.duongKinh);
        $("#input-suon").val(paramObj.suon);
        $("#input-salad").val(paramObj.salad);
        $("#input-pizza-type").val(paramObj.loaiPizza);
        $("#input-voucher").val(paramObj.idVourcher);
        $("#input-price").val(paramObj.thanhTien);
        $("#input-discount").val(paramObj.giamGia);
        $("#select-drink").val(paramObj.idLoaiNuocUong);
        $("#input-number-drink").val(paramObj.soLuongNuoc);
        $("#input-name").val(paramObj.hoTen);
        $("#input-email").val(paramObj.email);
        $("#input-phone").val(paramObj.soDienThoai);
        $("#input-address").val(paramObj.diaChi);
        $("#input-message").val(paramObj.loiNhan);
        $("#input-order-date").val(paramObj.ngayTao);
        $("#input-edit-date").val(paramObj.ngayCapNhat);
    }
    // Lọc đối tượng từ danh sách đơn hàng
    function filterData(paramObj){
        "use strict"
        var vFilterArr = []; // khai báo đối đã tượng lọc
        if (paramObj.trangThai != "all" && paramObj.loaiPizza == "all"){
            vFilterArr = gAllOrders.filter(function(item){
                if (item.trangThai != null){
                    return(item.trangThai.toLowerCase() === paramObj.trangThai);
                }
            });
        }
        if (paramObj.trangThai == "all" && paramObj.loaiPizza != "all"){
            vFilterArr = gAllOrders.filter(function(item){
                if (item.loaiPizza != null){
                    return(item.loaiPizza.toLowerCase() === paramObj.loaiPizza);
                }
            });
        }
        if (paramObj.trangThai == "all" && paramObj.loaiPizza == "all"){
            vFilterArr = gAllOrders;
            return vFilterArr;
        }
        if (paramObj.trangThai != "all" && paramObj.loaiPizza != "all") {
            vFilterArr = gAllOrders.filter(function(item){
                if (item.loaiPizza != null && item.trangThai != null){
                    return(item.trangThai.toLowerCase() === paramObj.trangThai && item.loaiPizza.toLowerCase() === paramObj.loaiPizza);
                }          
            });
        }
        return vFilterArr;
    }
    // Hàm thu thập dữ liệu cho vào đối tượng lọc
    function readDataToFilterObj(paramObj){
        "use strict"
        paramObj.trangThai = $("#select-order-status").val().toLowerCase();
        paramObj.loaiPizza = $("#select-pizza-type").val().toLowerCase();
    }
    // Hàm gọi Api lấy toàn bộ danh sách nước uống
    function callApiGetAllDrink(){
        "use strict"
        $.ajax({
            url: gBASE_URL_DRINK,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                gDrinkList = res; // Lưu dữ liệu trả về vào biến toàn cục
            },
            error: function(context){
                console.log(context.responseText);
            }
        });
    }
    // Hàm xử lí đưa dữ liệu nước uống vào ô select
    function handleDataDrinkToModal(paramArrDrink){
        "use strict"
        for (var bIndex = 0; bIndex < paramArrDrink.length; bIndex ++){
            $("#select-drink-create").append($("<option>", {
                value: paramArrDrink[bIndex].maNuocUong,
                text: paramArrDrink[bIndex].tenNuocUong
            }));
        };
    }
    // Hàm xử lí hiển thị dữ liệu vào bảng
    function handleDataToTable(paramArr){
        "use strict"
        gOrderTable.clear();
        gOrderTable.rows.add(paramArr);
        gOrderTable.draw();
    }
    // Hàm hiển thị dữ liệu combo được chọn lên modal khi chọn combo
    function loadComboToModal(paramDuongKinh, paramSuon, paramSalad, paramDrink, paramPrice){
        "use strict"
        $("#input-duong-kinh-create").val(paramDuongKinh);
        $("#input-suon-create").val(paramSuon);
        $("#input-salad-create").val(paramSalad);
        $("#input-number-drink-create").val(paramDrink);
        $("#input-price-create").val(paramPrice);
    }
    // Hàm xử lí lấy data từ hàng được nhấn
    function getDataRow(paramElement){
        "use strict"
        var vData = $(paramElement).closest("tr");
        var vDataOnRow = gOrderTable.row(vData).data();
        return vDataOnRow;
    }
});